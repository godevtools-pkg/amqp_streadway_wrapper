package amqp_streadway_wrapper

import (
	"github.com/streadway/amqp"
	"log"
	"time"
)

type Consumer struct {
	clientTag string
	broker    *Broker
}

func NewConsumer(clientTag string, broker *Broker) *Consumer {
	return &Consumer{
		clientTag: clientTag,
		broker:    broker,
	}
}

func (c *Consumer) GetDeliveryChannel(queueName, routingKey string) (<-chan amqp.Delivery, error) {

	if err := c.broker.Connect(); err != nil {
		return nil, err
	}

	err := c.broker.InitQueue(queueName, routingKey)
	if err != nil {
		return nil, err
	}

	deliveryChannel, err := c.broker.channel.Consume(queueName, c.clientTag, false, false, false, false, nil)
	if err != nil {
		return nil, err
	}

	return deliveryChannel, nil
}

func (c *Consumer) ListenQueue(queueName string, routingKey string, handler func([]byte) bool) {

	deliveryChannel, err := c.GetDeliveryChannel(queueName, routingKey)
	if err != nil {
		log.Panic(err)
	}

	c.Handle(deliveryChannel, handler, queueName, routingKey)
}

func (c *Consumer) Handle(
	deliveryChannel <-chan amqp.Delivery,
	handleFunc func([]byte) bool,
	queueName string,
	routingKey string,
) {

	for {

		for i := 0; i < 1; i++ {

			go func() {
				for msg := range deliveryChannel {
					if ok := handleFunc(msg.Body); ok {
						msg.Ack(false)
					} else {
						msg.Nack(false, false)
					}
				}
			}()
		}

		if err := <-c.broker.done; err != nil {
			for {
				//sleep before re-connect
				c.broker.Close()
				time.Sleep(30 * time.Second)

				var err error
				deliveryChannel, err = c.GetDeliveryChannel(queueName, routingKey)
				if err != nil {
					c.broker.logger.Errorf("reconnect err: %v", err)
				} else {
					break
				}
			}
			c.broker.logger.Info("reconnect finished")
		}

	}

}
