module gitlab.com/godevtools-pkg/amqp_streadway_wrapper

go 1.19

require (
	github.com/streadway/amqp v1.0.0
	gitlab.com/godevtools-pkg/logging v0.0.0-20221114172052-8b37838c9668
)

require (
	github.com/sirupsen/logrus v1.9.0 // indirect
	golang.org/x/sys v0.3.0 // indirect
)
