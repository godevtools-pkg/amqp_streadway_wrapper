package main

import (
	"fmt"
	mb "gitlab.com/godevtools-pkg/amqp_streadway_wrapper"
	"gitlab.com/godevtools-pkg/logging"
)

func main() {
	fmt.Println("Rabbit MQ")
	var connectionURL = "amqp://guest:guest@localhost:5672"

	logger := logging.GetLogger()
	broker := mb.NewBroker(connectionURL, "test-exchange", "topic", logger)
	err := broker.Connect()
	if err != nil {
		fmt.Println(err)
		return
	}

	//Consumer
	consumer := mb.NewConsumer("test-client", broker)
	consumer.ListenQueue("test-queue", "test-route", func(msg []byte) bool {
		fmt.Println("Message: ", string(msg))
		return true
	})

}
