package amqp_streadway_wrapper

import (
	"github.com/streadway/amqp"
	"gitlab.com/godevtools-pkg/logging"
	"time"
)

type Broker struct {
	connectionURL string
	exchangeName  string
	exchangeType  string
	logger        logging.Logger
	connection    *amqp.Connection
	channel       *amqp.Channel
	done          chan *amqp.Error
}

func NewBroker(connectionURL, exchangeName, exchangeType string, logger logging.Logger) *Broker {

	return &Broker{
		connectionURL: connectionURL,
		exchangeName:  exchangeName,
		exchangeType:  exchangeType,

		logger: logger,

		done: make(chan *amqp.Error),
	}
}

func (b *Broker) Connect() error {
	var err error
	b.connection, err = amqp.Dial(b.connectionURL)
	if err != nil {
		b.logger.Errorf(err.Error())
		return err
	}

	go func() {
		_ = <-b.connection.NotifyClose(b.done)
	}()

	blockings := b.connection.NotifyBlocked(make(chan amqp.Blocking))
	go func() {
		for block := range blockings {
			b.logger.Infof("TCP blocked: %t, reason: %s", block.Active, block.Reason)
		}
	}()

	b.channel, err = b.connection.Channel()
	if err != nil {
		b.logger.Errorf(err.Error())
		return err
	}

	err = b.InitExchange(b.exchangeName, b.exchangeType)
	if err != nil {
		b.logger.Errorf(err.Error())
		return err
	}

	return nil
}

func (b *Broker) Close() {

	if b.channel != nil {
		b.channel.Close()
		b.channel = nil
	}

	if b.connection != nil {
		b.connection.Close()
		b.connection = nil
	}

}

func (b *Broker) Reconnect() error {
	b.Close()

	time.Sleep(30 * time.Second)

	if err := b.Connect(); err != nil {
		return err
	}

	return nil
}

func (b *Broker) InitExchange(exchangeName, exchangeType string) error {
	err := b.channel.ExchangeDeclare(
		exchangeName,
		exchangeType,
		true,
		false,
		false,
		false,
		nil)

	if err != nil {
		b.logger.Errorf(err.Error())
		return err
	}

	return nil
}

func (b *Broker) InitQueue(queueName string, routingKey string) error {

	_, err := b.channel.QueueDeclare(queueName, true, false, false, false, nil)
	if err != nil {
		b.logger.Errorf(err.Error())
		return err
	}

	err = b.channel.QueueBind(queueName, routingKey, b.exchangeName, false, nil)
	if err != nil {
		b.logger.Errorf(err.Error())
		return err
	}

	return nil
}
