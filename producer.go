package amqp_streadway_wrapper

import (
	"github.com/streadway/amqp"
)

type Producer struct {
	broker *Broker
}

func NewProducer(broker *Broker) *Producer {
	return &Producer{
		broker,
	}
}

func (p *Producer) Publish(routingKey string, msg []byte, reliable bool) error {
	if reliable {
		p.broker.logger.Info("enabling publishing confirms.")
		if err := p.broker.channel.Confirm(false); err != nil {
			return err
		}

		confirms := p.broker.channel.NotifyPublish(make(chan amqp.Confirmation, 1))

		defer p.confirmOne(confirms)
	}

	if err := p.broker.channel.Publish(
		p.broker.exchangeName, // publish to an exchange
		routingKey,            // routing to 0 or more queues
		false,                 // mandatory
		false,                 // immediate
		amqp.Publishing{
			Headers:         amqp.Table{},
			ContentType:     "text/plain",
			ContentEncoding: "",
			Body:            msg,
			DeliveryMode:    amqp.Transient, // 1=non-persistent, 2=persistent
			Priority:        0,              // 0-9
		},
	); err != nil {
		return err
	}

	return nil
}

func (p *Producer) confirmOne(confirms <-chan amqp.Confirmation) {
	p.broker.logger.Info("waiting for confirmation of one publishing")
	if confirmed := <-confirms; confirmed.Ack {
		p.broker.logger.Infof("confirmed delivery with delivery tag: %d", confirmed.DeliveryTag)
	} else {
		p.broker.logger.Infof("failed delivery of delivery tag: %d", confirmed.DeliveryTag)
	}
}
